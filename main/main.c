//C:\Users\mateo\eclipse-workspace\robotic_arm
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "string.h"


#include "input_output.h"
#include "servo_motor.c"
#include "joystick_control.h"
#include "ultrasound.h"
#include "main.h"


void control_direction(servo_motor_config *motor_conf,int sense){

	float angleGet;

    if(sense==1) motor_conf->angle_prov = motor_conf->angle_prov + motor_conf->increment;	//Si estoy girando a la izquierda, resta angulo provisional
    else if(sense==2) motor_conf->angle_prov = motor_conf->angle_prov - motor_conf->increment; //Si estoy girando a la derecha, suma angulo provisional

    //Verifica los angulos minimos y maximos que puede tener el motor
    if(motor_conf->angle_prov <= motor_conf->min_angle) motor_conf->angle_prov = motor_conf->min_angle;		//Min angle
    if(motor_conf->angle_prov >= motor_conf->max_angle) motor_conf->angle_prov = motor_conf->max_angle;		//Max angle

	set_angle_servo_motor(*motor_conf,motor_conf->angle_prov);	//Envia el angulo deseado al motor

	angleGet = get_angle_servo_motor(*motor_conf);	//Lee el valor del angulo actual del motor

	for(int i=0;i<4;i++){	//Muestra angulo y rango del motor utilizado
	    if (motor_conf->id==i) ESP_LOGI("MOTOR","%s Angle: %d  Range: %d-%d",motor_conf->name,motor_conf->angle_prov,motor_conf->min_angle,motor_conf->max_angle);
	}

    if (motor_conf->angle_prov != angleGet){		//si el motor no se mueve a la posicion deseada, notifica error
    	ESP_LOGE("MOTOR","Error: AngleS: %d   AngleG: %.0f",motor_conf->angle,get_angle_servo_motor(*motor_conf));
    } else motor_conf->angle = motor_conf->angle_prov;	//Si se ha hecho correctamente, ya se puede guardar el valor del angulo real del motor

}

void motor_control_with_joystick(void *arg){

    int shoulder_joy_dir = 0;
    int base_joy_dir = 0;
    int elbow_joy_dir = 0;
    int hand_joy_dir = 0;

	while(1){

    	getJoystickValue(&shoulder_joy_dir,&base_joy_dir,&elbow_joy_dir,&hand_joy_dir);		//Lee el valor del joystick

    	control_direction(&shoulder_motor,shoulder_joy_dir);	//A partir de la lectura del joystick de base, le aplico al motor hand un valor
    	//control_direction(&base_motor,base_joy_dir);	//A partir de la lectura del joystick de base, le aplico al motor hand un valor
    	control_direction(&elbow_motor,elbow_joy_dir);	//A partir de la lectura del joystick de base, le aplico al motor hand un valor
    	control_direction(&hand_motor,hand_joy_dir);	//A partir de la lectura del joystick de hand, le aplico al motor hand un valor

        vTaskDelay(200 / portTICK_PERIOD_MS);
	}

}

void motor_control_with_ir(void *arg){

	float distance;

	while(1){

//		if (!getSensorRight()){		//Si detecta presencia en la derecha
//			control_direction(&shoulder_motor,1);	//Se mueve hacia la derecha
//			printf("Derecha \n");
//		}
//
//		if (!getSensorLeft()){		//Si detecta presencia en la izquierda
//			control_direction(&shoulder_motor,2);	//Se mueve hacia la izquierda
//			printf("Izquierda \n");
//		}

		getDataUS(&distance);
		if(distance >= 10 && distance <= 20){	//Si nos alejamos del sensor, queremos que el brazo se mueva hacia delante
			printf("Forward. Distance = %.1f \n",distance);
			control_direction(&shoulder_motor,1);	 //sumamos angulos shoulder
			if(elbow_motor.angle>=110) control_direction(&elbow_motor,1); //Restamos angulos elbow hasta quedarse fijo en 125
		} else if (distance < 8){	//Si nos acercamos al sensor, queremos que el brazo se mueva hacia detras
			printf("Backward. Distance = %.1f \n",distance);
			control_direction(&shoulder_motor,2);	//Se mueve hacia atras, restamos angulos shoulder
			if(elbow_motor.angle<=130) control_direction(&elbow_motor,2); //Sumamos angulo elbow hasta quedarse fijo en 170
		} else printf("Still. Distance = %.1f \n",distance);

        vTaskDelay(75 / portTICK_PERIOD_MS);

	}

}

void app_main(void)
{
    nvs_flash_init();

    servo_init();	 //Inicia la configuracion de los 4 servo motores
    joystick_init(); //Inicia la configuracion del joystick
    configInputs();	 //Inicia la configuracion de los sensores IR de proximidad
    configUS();

    vTaskDelay(1500 / portTICK_PERIOD_MS);

    //xTaskCreate(motor_control_with_joystick, "Joystick control motors", 1024 * 4, (void *)0, 10, NULL);	//Tarea control de motor con joystick
    xTaskCreate(motor_control_with_ir, "Joystick control motors", 1024 * 4, (void *)0, 10, NULL);	//Tarea control de motor con joystick

    while (1) {

        vTaskDelay(100 / portTICK_PERIOD_MS);

    }
}

