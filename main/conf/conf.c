/*
 * config.c
 *
 *  Created on: 12 nov. 2021
 *      Author: PC
 */

#include "conf.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <stdio.h>
#include "time.h"
#include <time.h>
#include "string.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_attr.h"
#include "esp_sntp.h"


static const char *TAG = "CONF";
uint8_t my_sntp_initialized = 0;

/*
 * En config.c y config.h:
 * - RTC timer (reloj del ESP32)
 * - SNTP (hora real de internet)
 * - Timers
 */

//To set the timezone (TZ) function
void rtc_time_start_timezone(char * tz){

	if (getenv((char*)"TZ") == NULL) {
		ESP_LOGI(TAG, "Set timezone to: %s", tz);
		setenv("TZ", tz, 1);
	} else {
		ESP_LOGI(TAG, "Use timezone : %s", getenv("TZ"));
	}

}

//To get the current time broken down in a tm structure
void rtc_time_get_now_with_tz(struct tm * time_now){

	memset(time_now, 0, sizeof(struct tm));
	time_t now;
	tzset();
	time(&now);
	localtime_r(&now, time_now);

}

void sntp_sync_time(struct timeval *tv)
{
	settimeofday(tv, NULL);
	//ESP_LOGW(TAG, "Time is synchronized from custom code");
	sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
}

void time_sync_notification_cb(struct timeval *tv)
{
	ESP_LOGI(TAG, "Notification of a time synchronization event");
}

//Inicia sntp. Se conecta a la red para obtener la hora actual
void sntp_wrap_init(void)
{

	if(my_sntp_initialized) return;

	ESP_LOGI(TAG, "Initializing SNTP");
	sntp_setoperatingmode(SNTP_OPMODE_POLL);
	sntp_setservername(0, "pool.ntp.org");
	sntp_set_time_sync_notification_cb(time_sync_notification_cb);
	sntp_init();

	my_sntp_initialized = 1;

}

void sntp_wrap_stop(void){
	ESP_LOGE(TAG, "SNTP stop not implemented");
}


