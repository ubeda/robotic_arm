/* servo motor control example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_attr.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include "esp_err.h"
#include "math.h"

#include "soc/mcpwm_periph.h"
#include "driver/mcpwm.h"

#include "servo_motor.h"


//SERVOMOTOR DS-R005 SER0056
//You can get these value from the datasheet of servo you use, in general pulse width varies between 1000 to 2000 mocrosecond
//#define SERVO_MIN_PULSEWIDTH 500 //Minimum pulse width in microsecond	1000
//#define SERVO_MAX_PULSEWIDTH 2500 //Maximum pulse width in microsecond	2000
//#define SERVO_MAX_DEGREE 300 //Maximum angle in degree upto which servo can rotate



//SERVOMOTOR SG90
//You can get these value from the datasheet of servo you use, in general pulse width varies between 1000 to 2000 mocrosecond
#define SERVO_MIN_PULSEWIDTH 400 //Minimum pulse width in microsecond	400
#define SERVO_MAX_PULSEWIDTH 2400 //Maximum pulse width in microsecond	2400
#define SERVO_MAX_DEGREE 180 //Maximum angle in degree upto which servo can rotate 180


uint32_t pulse_width;
esp_err_t err;
bool auto_activated = false;

/**
 * @brief Configure MCPWM module
 */

servo_motor_config base_motor;
servo_motor_config shoulder_motor;
servo_motor_config elbow_motor;
servo_motor_config hand_motor;

void servo_init(void){	//Definimos la configuracion de cada motor y llamamos a servo_control_init() para cada motor

	base_motor.name = "BASE";
	base_motor.pin = 5;
	base_motor.mcpwm_num = MCPWM_UNIT_0;
	base_motor.io_signal = MCPWM0A;
	base_motor.timer_num = MCPWM_TIMER_0;
	base_motor.gen = MCPWM_OPR_A;
	base_motor.id = 0;
	base_motor.angle_prov = 90;
	base_motor.increment = 5;
	base_motor.min_angle = 85;
	base_motor.max_angle = 110;
	servo_control_init(base_motor);

	shoulder_motor.name = "SHOULDER";
	shoulder_motor.pin = 14;
	shoulder_motor.mcpwm_num = MCPWM_UNIT_0;
	shoulder_motor.io_signal = MCPWM1A;
	shoulder_motor.timer_num = MCPWM_TIMER_1;
	shoulder_motor.gen = MCPWM_OPR_A;
	shoulder_motor.id = 1;
	shoulder_motor.angle_prov = 140;
	shoulder_motor.increment = 5;
	shoulder_motor.min_angle = 85;
	shoulder_motor.max_angle = 190;
	servo_control_init(shoulder_motor);

	elbow_motor.name = "ELBOW";
	elbow_motor.pin = 23;
	elbow_motor.mcpwm_num = MCPWM_UNIT_1;
	elbow_motor.io_signal = MCPWM1A;
	elbow_motor.timer_num = MCPWM_TIMER_1;
	elbow_motor.gen = MCPWM_OPR_A;
	elbow_motor.id = 2;
	elbow_motor.angle_prov = 145;
	elbow_motor.increment = -5;
	elbow_motor.min_angle = 75;
	elbow_motor.max_angle = 185;
	servo_control_init(elbow_motor);

	hand_motor.name = "HAND";
	hand_motor.pin = 4;
	hand_motor.mcpwm_num = MCPWM_UNIT_1;
	hand_motor.io_signal = MCPWM2A;
	hand_motor.timer_num = MCPWM_TIMER_2;
	hand_motor.gen = MCPWM_OPR_A;
	hand_motor.id = 3;
	hand_motor.angle_prov = 97;
	hand_motor.increment = 2;
	hand_motor.min_angle = 85;
	hand_motor.max_angle = 110;
	servo_control_init(hand_motor);


}

void servo_control_init(servo_motor_config motor_conf){		//Configuracion de un motor. Se necesita llamar a esta funcion para cada motor

    //1. mcpwm gpio initialization
    mcpwm_gpio_init(motor_conf.mcpwm_num, motor_conf.io_signal, motor_conf.pin);    //Set GPIO 18 as PWM0A, to which servo is connected

    //2. initial mcpwm configuration
    ESP_LOGI("Servo-motor","Configuring Initial Parameters of mcpwm 0");
    mcpwm_config_t pwm_config;
    pwm_config.frequency = 50;    //frequency = 50Hz, i.e. for every servo motor time period should be 20ms
    pwm_config.cmpr_a = 0;    //duty cycle of PWMxA = 0
    pwm_config.cmpr_b = 0;    //duty cycle of PWMxb = 0
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    err = mcpwm_init(motor_conf.mcpwm_num, motor_conf.timer_num, &pwm_config);    //Configure PWM0A & PWM0B with above settings
    if(err!=0) ESP_LOGE("Servo-motor","Can't Configure Initial Parameters of mcpwm 0");

}


static uint32_t degree_to_pulsewidth(uint32_t degree_of_rotation)
{
    uint32_t cal_pulsewidth = 0;
    cal_pulsewidth = (SERVO_MIN_PULSEWIDTH + (((SERVO_MAX_PULSEWIDTH - SERVO_MIN_PULSEWIDTH) * (degree_of_rotation)) / (SERVO_MAX_DEGREE)));
    return cal_pulsewidth;
}


esp_err_t set_angle_servo_motor(servo_motor_config motor_conf, uint32_t angle){ //Ex: set_angle_servo_motor(shoulder,90)
	if(angle>=0 && angle<=200){
		pulse_width = degree_to_pulsewidth(angle);
		//printf("pulse_width: %d\n", pulse_width);
		err = mcpwm_set_duty_in_us(motor_conf.mcpwm_num, motor_conf.timer_num, motor_conf.gen, pulse_width);
	    if(err!=0) ESP_LOGE("Servo-motor","Can't Set angle correctly");
	    vTaskDelay(10);     //Add delay, since it takes time for servo to rotate, generally 100ms/60degree rotation at 5V
	} else ESP_LOGE("Servo-motor","Can't move to that angle");
	return err;
}

void set_angle_servo_motor_365(void *arg){

	while(1){
		if(auto_activated){		//Si el modo auto esta activado
			for(int angle=0;angle<300;angle=angle+5){	//Giramos en un sentido de 0 a 300 grados
				if(auto_activated){		//Si desactivamos el modo auto en mitad de un bucle, gracias a este if, se interrumpiria el bucle
					pulse_width = degree_to_pulsewidth(angle);
					err = mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, pulse_width);
					vTaskDelay(10);
				} else break;
			}

			for(int angle=300;angle>0;angle=angle-5){	//Giramos en el otro sentido de 300 a 0 grados
					if(auto_activated){	    //Si desactivamos el modo auto en mitad de un bucle, gracias a este if, se interrumpiria el bucle
					pulse_width = degree_to_pulsewidth(angle);
					err = mcpwm_set_duty_in_us(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, pulse_width);
					vTaskDelay(10);
				} else break;
			}
		} else	vTaskDelay(300);

	}
}

float get_angle_servo_motor(servo_motor_config motor_conf){
	float duty = 0;
	float angle = 0;
	duty = mcpwm_get_duty(motor_conf.mcpwm_num, motor_conf.timer_num, motor_conf.gen);	//Devuelve 2.5 para 0 grados y 12.5 para 300 grados
	//angle = round((30*duty) - 75);		//Regla de 3 hecha a mano para el servomotor DS-R005 SER0056
	//angle = round((20*duty) - 60);		//Devuelve 3 para 0 grados y 13 para 200 grados para SG90
	angle = round((18.018*duty) - 36.035);		//Devuelve 3 para 0 grados y 13 para 200 grados para SG90

	//printf("Duty Cycle get: %.2f - Angle get: %.2f \n",duty,angle);
	return angle;
}


