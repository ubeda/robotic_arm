/*
 * buzzer.c
 *
 *  Created on: 15 jun. 2020
 *      Author: dismuntel
 */

#include "string.h"
#include "esp_event.h"
#include "esp_timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "sdkconfig.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"

#include "led.h"

#define LEDC_HS_TIMER          LEDC_TIMER_2
#define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
#define LEDC_TEST_CH_NUM       (4)
#define MAX_LIGHT			   4095

static const char *TAG = "LED";

ledc_channel_config_t led_channel1 = {
        .channel    = LEDC_CHANNEL_0,
        .duty       = 0,
        .gpio_num   = CONFIG_LED_R_PIN,
        .speed_mode = LEDC_HS_MODE,
        .hpoint     = 0,
        .timer_sel  = LEDC_HS_TIMER
};

ledc_channel_config_t led_channel2 = {
        .channel    = LEDC_CHANNEL_1,
        .duty       = 0,
        .gpio_num   = CONFIG_LED_G_PIN,
        .speed_mode = LEDC_HS_MODE,
        .hpoint     = 0,
        .timer_sel  = LEDC_HS_TIMER
};

ledc_channel_config_t led_channel3 = {
        .channel    = LEDC_CHANNEL_2,
        .duty       = 0,
        .gpio_num   = CONFIG_LED_B_PIN,
        .speed_mode = LEDC_HS_MODE,
        .hpoint     = 0,
        .timer_sel  = LEDC_HS_TIMER
};


void initLedTricolor() {


    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_8_BIT, 	//resolution of PWM duty  (2^8 bits - 1) = 256-1 = 255
        .freq_hz = 1000,                      	//frequency of PWM signal
        .speed_mode = LEDC_HS_MODE,           	//timer mode
        .timer_num = LEDC_HS_TIMER            	//timer index
    };

    // Set configuration of timer0 for high speed channels
    ledc_timer_config(&ledc_timer);
    // Set LED Controller with previously prepared configuration
    ledc_channel_config(&led_channel1);
    ledc_channel_config(&led_channel2);
    ledc_channel_config(&led_channel3);

    //Apagamos el LED
    ledc_stop(LEDC_HS_MODE, LEDC_CHANNEL_0, 0);
    ledc_stop(LEDC_HS_MODE, LEDC_CHANNEL_1, 0);
    ledc_stop(LEDC_HS_MODE, LEDC_CHANNEL_2, 0);
    // Initialize fade service.
    ESP_LOGI("PWM", "Hemos inicializado el PWM");
	ledc_fade_func_install(0);
}


uint32_t getDutyLED(void) {	//Realiza una lectura del LED para saber su duty actual

   uint32_t current_duty;

   current_duty = ledc_get_duty(led_channel1.speed_mode, led_channel1.channel);
   printf("Current duty: %d \n",(int) current_duty);

   return current_duty;
}

void set_duty_led(uint32_t duty, int mode) { //Enciende el LED a pi�on fijo

	esp_err_t err;

	switch(mode){

		case 0:  //LED 0
			ESP_LOGI(TAG,"Enciendo LED 0");
			ledc_set_duty(led_channel1.speed_mode, led_channel1.channel, duty);
			err = ledc_update_duty(led_channel1.speed_mode, led_channel1.channel);
			if(err != ESP_OK) ESP_LOGE(TAG,"Error en el LED 1. err: %d",err);
			break;

		case 1:  //LED 1
			ESP_LOGI(TAG,"Enciendo LED 1");
			ledc_set_duty(led_channel2.speed_mode, led_channel2.channel, duty);
			err = ledc_update_duty(led_channel2.speed_mode, led_channel2.channel);
			if(err != ESP_OK) ESP_LOGE(TAG,"Error en el LED 2. err: %d",err);
			break;

		case 2:  //LED 2
			ESP_LOGI(TAG,"Enciendo LED 2");
			ledc_set_duty(led_channel3.speed_mode, led_channel3.channel, duty);
			err = ledc_update_duty(led_channel3.speed_mode, led_channel3.channel);
			if(err != ESP_OK) ESP_LOGE(TAG,"Error en el LED 3. err: %d",err);
			break;

		default:
			break;
	}
}

void apaga_todo (void){
	ledc_set_duty(led_channel1.speed_mode, led_channel1.channel, 0);
	ledc_update_duty(led_channel1.speed_mode, led_channel1.channel);

	ledc_set_duty(led_channel2.speed_mode, led_channel2.channel, 0);
	ledc_update_duty(led_channel2.speed_mode, led_channel2.channel);

	ledc_set_duty(led_channel3.speed_mode, led_channel3.channel, 0);
	ledc_update_duty(led_channel3.speed_mode, led_channel3.channel);
}

void enciende_1 (void){
	ledc_set_duty(led_channel1.speed_mode, led_channel1.channel, 255);
	ledc_update_duty(led_channel1.speed_mode, led_channel1.channel);
}

void enciende_2 (void){
	ledc_set_duty(led_channel2.speed_mode, led_channel2.channel, 255);
	ledc_update_duty(led_channel2.speed_mode, led_channel2.channel);
}

void enciende_3 (void){
	ledc_set_duty(led_channel3.speed_mode, led_channel3.channel, 255);
	ledc_update_duty(led_channel3.speed_mode, led_channel3.channel);
}

void set_duty_led_with_fade(uint32_t duty, int fade_time, uint8_t mode) { //Enciende el LED progresivamente

	switch(mode){

		case 0:
			printf("Encendido progesivo \n");
			ledc_set_fade_with_time(led_channel1.speed_mode,led_channel1.channel, duty, 100);
			ledc_fade_start(led_channel1.speed_mode,led_channel1.channel, LEDC_FADE_NO_WAIT);
			vTaskDelay(pdMS_TO_TICKS(150));
			break;

		case 1:
			printf("Apagado progesivo \n");
			ledc_set_fade_with_time(led_channel1.speed_mode,led_channel1.channel, duty, 100);
			ledc_fade_start(led_channel1.speed_mode,led_channel1.channel, LEDC_FADE_NO_WAIT);
			vTaskDelay(pdMS_TO_TICKS(150));
			break;

	}
}
