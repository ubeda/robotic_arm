

/*
 * servo_motor.h
 *
 *  Created on: 25 dic. 2021
 *      Author: PC
 */

#ifndef MAIN_PWM_SERVO_MOTOR_H_
#define MAIN_PWM_SERVO_MOTOR_H_

extern bool auto_activated;

typedef struct{

	int pin;
	mcpwm_unit_t mcpwm_num;
	mcpwm_io_signals_t io_signal;
	mcpwm_timer_t timer_num;
	mcpwm_generator_t gen;
	int angle;
	int angle_prov;
	char *name;
	int id;
	int increment;
	int min_angle;
	int max_angle;

}__attribute__((packed)) servo_motor_config;

extern servo_motor_config shoulder_motor;
extern servo_motor_config base_motor;
extern servo_motor_config elbow_motor;
extern servo_motor_config hand_motor;

void servo_init(void);
void servo_control_init(servo_motor_config motor_conf);
static uint32_t degree_to_pulsewidth(uint32_t degree_of_rotation);
void set_angle_servo_motor_365(void *arg);
float get_angle_servo_motor(servo_motor_config motor_conf);

#endif /* MAIN_PWM_SERVO_MOTOR_H_ */
