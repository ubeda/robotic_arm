/*
 * main.h
 *
 *  Created on: 10 feb 2022
 *      Author: asus
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

void motor_control_with_joystick(void *arg);
void motor_control_with_ir(void *arg);
void app_main(void);


#endif /* MAIN_MAIN_H_ */
