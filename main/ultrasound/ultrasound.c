/*
 * ultrasound.c
 *
 *  Created on: 21 feb 2023
 *      Author: mateo
 */


#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "stdbool.h"
#include "driver/gpio.h"
#include "hal/gpio_types.h"
#include <stdio.h>
#include <time.h>

#include "ultrasound.h"

#define MASK_ECHO  ((1ULL<<GPIO_NUM_18))
#define MASK_TRIGGER  ((1ULL<<GPIO_NUM_19))
#define measures 15

/*
 * La velocidad del sonido = 340 m/s = 0.34 cm/us
 * velocidad = 2*distancia/tiempo --> v = 2*d/t
 * Despejando: d = t*v/2 = t*0.034/2 = t * 0.017 = t/58.823 = t/59
 *
 * No se puede usar la funcion vTaskDelay(1 / portTICK_PERIOD_MS) para delats de
 * microsegundos, debido a que delays inferiores a 1 ms se interpretar�n como 0 ms y por lo tanto
 * no efectua ningun delay. La solucion es usar ets_delay_us(), la cual bloquea la CPU durante esos us.
 *
 * Para timers de us no se puede usar la funcion clock(), pues no detecta microsegundos. Se debe usar
 * la funcion esp_timer_get_time().
 *
 */

void configUS (void){

	//Echo	- Input
	gpio_config_t echo;
	echo.pin_bit_mask = MASK_ECHO;
	echo.mode = GPIO_MODE_INPUT;
	echo.intr_type = GPIO_INTR_DISABLE;
	echo.pull_down_en = GPIO_PULLDOWN_DISABLE;
	echo.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&echo);

	//Trigger	- Output
	gpio_config_t trigger;
	trigger.pin_bit_mask = MASK_TRIGGER;
	trigger.mode = GPIO_MODE_OUTPUT;
	trigger.intr_type = GPIO_INTR_DISABLE;
	trigger.pull_down_en = GPIO_PULLDOWN_DISABLE;
	trigger.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&trigger);

	ESP_LOGI("US","Ultrasound configuration successfull");

}

void getDataUS (float *dist){

	long long int times;
	float dist_buffer [measures];
	float summatory = 0;

	for(int i=0; i< measures; i++){

		gpio_set_level(GPIO_NUM_19,1);	//Envia una se�al de high al trigger durante 10 microsegundos
		ets_delay_us(10);
		gpio_set_level(GPIO_NUM_19,0);	//Tras los 10 us, envia un low al trigger

		//Falta introducir un cortafuegos para salir del while para que no explote el programa
		while((gpio_get_level(GPIO_NUM_18) == 0)){  //Espera hasta que el echo se ponga a high
			ets_delay_us(1);
		}

		times = esp_timer_get_time();	//Cuando el echo se ponga a high, empieza el contador

		while(gpio_get_level(GPIO_NUM_18) == 1){	//Mientras Echo este a 1, sigue el contador
			ets_delay_us(1);
		}

		times = esp_timer_get_time() - times;	//Cuando echo vuelva a 0, para el timer

		dist_buffer[i] = times*0.017;  //Calcula la distancia
		summatory += dist_buffer[i];
		//printf("Distancia [%d]: %.1f \n",i,dist_buffer[i]);

	}

	*dist = summatory/measures;
	//printf("Distancia: %.1f \n",*dist);

	vTaskDelay(75 / portTICK_PERIOD_MS);

}

