/*
 * input_output.c
 *
 *  Created on: 14 may. 2021
 *      Author: mateo.ubeda
 */

#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
//#include "hal_utils.h"
//#include "hal_debounce.h"
#include "stdbool.h"
#include "driver/gpio.h"
#include "hal/gpio_types.h"

#include "input_output.h"

//#define GPIO_BIT_MASK_POWER  (1ULL<<GPIO_NUM_25)
//#define GPIO_BIT_MASK_OUTPUTS  ((1ULL<<GPIO_NUM_22) | (1ULL<<GPIO_NUM_23))
#define GPIO_BIT_MASK_INPUTS_BUTTON_JOYSTICKS  ((1ULL<<GPIO_NUM_26) | (1ULL<<GPIO_NUM_2))
#define GPIO_BIT_MASK_INPUTS_IR_SENSORS  ((1ULL<<GPIO_NUM_27) | (1ULL<<GPIO_NUM_15))


void configInputs(void){

	//Configura los dos botones del los dos joysticks
	gpio_config_t inputs_button;
	inputs_button.pin_bit_mask = GPIO_BIT_MASK_INPUTS_BUTTON_JOYSTICKS;
	inputs_button.mode = GPIO_MODE_INPUT;
	inputs_button.intr_type = GPIO_INTR_DISABLE;
	inputs_button.pull_down_en = GPIO_PULLDOWN_DISABLE;
	inputs_button.pull_up_en = GPIO_PULLUP_ENABLE;
	gpio_config(&inputs_button);

	//Cofigura las dos entradas de los sensores de IR
	gpio_config_t inputs_irSensor;
	inputs_irSensor.pin_bit_mask = GPIO_BIT_MASK_INPUTS_IR_SENSORS;
	inputs_irSensor.mode = GPIO_MODE_INPUT;
	inputs_irSensor.intr_type = GPIO_INTR_DISABLE;
	inputs_irSensor.pull_down_en = GPIO_PULLDOWN_DISABLE;
	inputs_irSensor.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&inputs_irSensor);

}

int getSensorRight (void){

	return gpio_get_level(GPIO_NUM_27);

}

int getSensorLeft (void){

	return gpio_get_level(GPIO_NUM_15);

}

int getJoystickButton1 (void){

	return gpio_get_level(GPIO_NUM_26);

}

int getJoystickButton2 (void){

	return gpio_get_level(GPIO_NUM_2);

}




