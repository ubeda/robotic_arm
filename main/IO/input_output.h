/*
 * input_output.h
 *
 *  Created on: 14 may. 2021
 *      Author: mateo.ubeda
 */

#ifndef MAIN_IO_INPUT_OUTPUT_H_

#define MAIN_IO_INPUT_OUTPUT_H_


void configInputs(void);
int getSensorRight (void);
int getSensorLeft (void);
int getJoystickButton1(void);
int getJoystickButton2(void);



#endif /* MAIN_IO_INPUT_OUTPUT_H_ */
