/*
 * joystick_control.c
 *
 *  Created on: 26 feb 2022
 *      Author: mateo
 */

#include <stdio.h>
#include <stdlib.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "input_output.h"

#include "joystick_control.h"

/*
 * Cada joystick tiene un button que es una entrada digital. Se declara en "input_output.h" y necesitan que
 * se active el pull up en la congiguracion porque cuando se pulsan, el pin digital se conecta a GND, pero cuando no se pulsa
 * no esta conectado a nada. Por eso, el pull up hace que cuando no este pulsado este conectado a VCC y cuando
 * se pulse este conectado a GND
 */



/*
 * Los canales de un mismo ADC se transfieren el valor cuando llega al maximo. Es decir, si configuro channel 1 y channel 4 para ADC1,
 * cuando mueva el channel 1 a un extremo, el channel 4 cogera su valor. Como solo hay dos ADC (ADC1 y ADC2) y necesitamos 4 canales,
 * necesariamente usamos 2 canales de ADC1 y 2 canales de ADC2.
 * La estrategia para que el problema no afecte es poner un canal de ADC1 y otro de ADC2 para cada joystick y hacer uso de la variable
 * 'control' para solo darle poder de control a un joystick. Este poder se activa tras pulsar el boton del joystick y solo se podra usar este.
 * Para usar el otro joystick, hara falta pulsar su boton.
 *
 */


//Los pines 0,2,4, 12 y 15 no se pueden usar para ADC
//ADC Channels
#if CONFIG_IDF_TARGET_ESP32
#define ADC1_EXAMPLE_CHAN0          ADC1_CHANNEL_3  //SENSOR_VN - GPIO39  -- Shoulder
#define ADC2_EXAMPLE_CHAN0          ADC2_CHANNEL_4	//GPIO13 -- Base

#define ADC1_EXAMPLE_CHAN1          ADC1_CHANNEL_4  //GPIO32 -- Elbow
#define ADC2_EXAMPLE_CHAN1          ADC2_CHANNEL_8	//GPIO25 -- Hand

#else
#define ADC1_EXAMPLE_CHAN0          ADC1_CHANNEL_2
#define ADC2_EXAMPLE_CHAN0          ADC2_CHANNEL_0
static const char *TAG_CH[2][10] = {{"ADC1_CH2"}, {"ADC2_CH0"}};
#endif

static const char *TAG_JOY = "JOYSTICK";

//ADC Attenuation
#define ADC_EXAMPLE_ATTEN           ADC_ATTEN_DB_11

//ADC Calibration
#if CONFIG_IDF_TARGET_ESP32
#define ADC_EXAMPLE_CALI_SCHEME     ESP_ADC_CAL_VAL_EFUSE_VREF
#elif CONFIG_IDF_TARGET_ESP32S2
#define ADC_EXAMPLE_CALI_SCHEME     ESP_ADC_CAL_VAL_EFUSE_TP
#elif CONFIG_IDF_TARGET_ESP32C3
#define ADC_EXAMPLE_CALI_SCHEME     ESP_ADC_CAL_VAL_EFUSE_TP
#elif CONFIG_IDF_TARGET_ESP32S3
#define ADC_EXAMPLE_CALI_SCHEME     ESP_ADC_CAL_VAL_EFUSE_TP_FIT
#endif


static int adc_raw[2][10];
static int adc_raw1[2][10];
static const char *TAG = "ADC SINGLE";

static esp_adc_cal_characteristics_t adc1_chars;
static esp_adc_cal_characteristics_t adc2_chars;

//#define centerValue 2900
int control = 0;
int centerValue;	//No se puede poner fijo porque dependendiendo de la fuende de tension, el valor va a variar
int x_axis1;
int y_axis1;
int x_axis2;
int y_axis2;

static bool adc_calibration_init(void)
{
    esp_err_t ret;
    bool cali_enable = false;

    ret = esp_adc_cal_check_efuse(ADC_EXAMPLE_CALI_SCHEME);
    if (ret == ESP_ERR_NOT_SUPPORTED) {
        ESP_LOGW(TAG, "Calibration scheme not supported, skip software calibration");
    } else if (ret == ESP_ERR_INVALID_VERSION) {
        ESP_LOGW(TAG, "eFuse not burnt, skip software calibration");
    } else if (ret == ESP_OK) {
        cali_enable = true;
        esp_adc_cal_characterize(ADC_UNIT_1, ADC_EXAMPLE_ATTEN, ADC_WIDTH_BIT_DEFAULT, 0, &adc1_chars);
        esp_adc_cal_characterize(ADC_UNIT_2, ADC_EXAMPLE_ATTEN, ADC_WIDTH_BIT_DEFAULT, 0, &adc2_chars);
    } else {
        ESP_LOGE(TAG, "Invalid arg");
    }

    return cali_enable;
}

void joystick_init(void)
{

    ESP_ERROR_CHECK(adc1_config_width(ADC_WIDTH_BIT_DEFAULT)); //Esto solo se hace para el ADC1. Esta conf para ADC2 se mete en adc2_get_raw()

	//Joystick 1
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_EXAMPLE_CHAN0, ADC_EXAMPLE_ATTEN)); //Configura ADC1_CHANNEL_ (GPIO)
    ESP_ERROR_CHECK(adc2_config_channel_atten(ADC2_EXAMPLE_CHAN0, ADC_EXAMPLE_ATTEN)); //Configura ADC2_CHANNEL_ (GPIO)

	//Joystick 2
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_EXAMPLE_CHAN1, ADC_EXAMPLE_ATTEN)); //Configura ADC1_CHANNEL_ (GPIO)
    ESP_ERROR_CHECK(adc2_config_channel_atten(ADC2_EXAMPLE_CHAN1, ADC_EXAMPLE_ATTEN)); //Configura ADC2_CHANNEL_ (GPIO)

    adc_calibration_init();

    centerValue = adc1_get_raw(ADC1_EXAMPLE_CHAN0);	//Leo el valor para cuando el joystick este en el centro
    x_axis1 = centerValue;
    y_axis1 = centerValue;
    x_axis2 = centerValue;
    y_axis2 = centerValue;

}


void getJoystickValue(int *shoulderS, int *baseS, int *elbowS, int *handS){

	//Joystick 1
	adc_raw[0][0] = adc1_get_raw(ADC1_EXAMPLE_CHAN0);	//Leo el valor del eje x del Joystick 1, que controlara a Shoulder
	adc2_get_raw(ADC2_EXAMPLE_CHAN0, ADC_WIDTH_BIT_DEFAULT, &adc_raw[1][0]); ////Leo el valor del eje y del Joystick 1, que controlara a Base

	//Joystick 2
	adc_raw1[0][0] = adc1_get_raw(ADC1_EXAMPLE_CHAN1);  ////Leo el valor del eje x del Joystick 2, que controlara a Elbow
	adc2_get_raw(ADC2_EXAMPLE_CHAN1, ADC_WIDTH_BIT_DEFAULT, &adc_raw1[1][0]); //Leo el valor del eje y del Joystick 2, que controlara a Hand

	if(!getJoystickButton1()) control = 1; 	//Si se acciona el pulsador del joystick 1, le doy el control al joystick 1
	if(!getJoystickButton2()) control = 2; 	//Si se acciona el pulsador del joystick 2, le doy el control al joystick 2

	if(control == 1){					//Cuando el joystick 1 tenga el control, se guardan sus lecturas y se pone el joystick 2 por defecto
		x_axis1 = adc_raw[0][0];		//Me guardo lectura Shoulder
		y_axis1 = adc_raw[1][0];		//Me guardo lectura Base
		x_axis2 = centerValue;			//Pongo por defecto Elbow
		y_axis2 = centerValue;			//Pongo por defecto Hand
	} else if(control == 2){		    //Cuando el joystick 2 tenga el control, se guardan sus lecturas y se pone el joystick 1 por defecto
		x_axis1 = centerValue;			//Pongo por defecto Shoulder
		y_axis1 = centerValue;			//Pongo por defecto Base
		x_axis2 = adc_raw1[0][0];		//Me guardo lectura Elbow
		y_axis2 = adc_raw1[1][0];		//Me guardo lectura Hand
	}

	//0 -> joystick centro, 1 -> joystick derecha, 2 -> joystick izquierda

	if(x_axis1<=0.80*centerValue) *shoulderS = 1;
	else if (x_axis1>=1.2*centerValue) *shoulderS = 2;
	else *shoulderS = 0;

	if(y_axis1<=0.80*centerValue) *baseS = 1;
	else if (y_axis1>=1.2*centerValue) *baseS = 2;
	else *baseS = 0;

	if(x_axis2<=0.80*centerValue) *elbowS = 1;
	else if (x_axis2>=1.2*centerValue) *elbowS = 2;
	else *elbowS = 0;

	if(y_axis2<=0.80*centerValue) *handS = 1;
	else if (y_axis2>=1.2*centerValue) *handS = 2;
	else *handS = 0;

	//printf("Shoulder: %d , Base: %d , Elbow: %d , Hand: %d , centerValue: %d \n", x_axis1,y_axis1,x_axis2,y_axis2,centerValue);
	printf("Shoulder: %d , Base: %d , Elbow: %d , Hand: %d, centerValue: %d \n",*shoulderS,*baseS,*elbowS,*handS,centerValue);


}
