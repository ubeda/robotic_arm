/*
 * joystick_control.h
 *
 *  Created on: 26 feb 2022
 *      Author: asus
 */

#ifndef MAIN_JOYSTICK_JOYSTICK_CONTROL_H_
#define MAIN_JOYSTICK_JOYSTICK_CONTROL_H_

extern int centerValue;

void joystick_init(void);
void getJoystickValue(int *shoulderS, int *baseS, int *elbowS, int *handS);

#endif /* MAIN_JOYSTICK_JOYSTICK_CONTROL_H_ */
