Robotic Arm
====================

This project aims to control an specific type of robotic arm in which 4 servo-motors are used. Besides, some extra peripherals
will be used so as to send specific data to the motors. The microcontroller used in this project is the ESP32 from Espressif, which 
will be the core of the robot and the intermediary of the motor and the peripherals.

Currently the motors are controlled with 2 joysticks, but there is a further plan to control them with the help of ultra-sound sensors,
IR-sensors and even gyroscopes. Also, Bluetooth and Wi-Fi technologies will be added soon.



